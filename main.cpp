#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <sstream>  
#include <vector>
#include <stdexcept>
#include <time.h> 
#include <chrono> 

using namespace std;


#define accuracy 0.8

struct Result
{
	int id;
	int price_reached;
	char* items;
	Result(int id, int items_amount){
		this->id = id;
		price_reached = 0;
		items = new char[items_amount];
	}
	~Result(){
		delete[] items;
	}

};

struct Backpack{
	int id;
	int  items_amount;
	int backpack_capacity;
	int *backpack_items;
	int *items_values;
	Result* result;
	Backpack(int items_amount){
		this->items_amount = items_amount;
		backpack_items = new int[items_amount];
		items_values = new int[items_amount];
	};
	~Backpack(){
		delete[] backpack_items;
		delete[] items_values;
		delete result;
		// cout<<id<<" byl jsem zabit"<<endl;
	};
};
class Tester
{
public:
	Tester(){};
	~Tester();
	void loadData(ifstream *input);
	void loadResults(ifstream *input);
	void runTests(int n);
	void runTestsBB(int n);
	void runTestsDyn(int n);
	void runTestsFPTAS(int n);
private:
	vector<Backpack*> backpacks;	
};

class BackpackLoader
{
public:
	BackpackLoader(){};
	~BackpackLoader(){};
	Backpack* loadData(string data);
	
};


class StupidSolver
 {
 public:
 	StupidSolver(){};
 	~StupidSolver(){};
 	Result* solve(Backpack* bag);
 	void solveRecursively(const Backpack* bag,int begin,Result* bestResult, Result* actualResult);
 	/* data */
 }; 

 

Backpack* BackpackLoader::loadData(string data){
	stringstream ss; 
	ss<<data;
	int count,id;
	ss>>id;
	//cout<<id<<endl;
	ss>>count;
	Backpack* bag = new Backpack(count);
	bag->id = id;
	ss>>bag->backpack_capacity;
	for (int i=0;i<count;i++){
		ss>>bag->backpack_items[i];
		ss>>bag->items_values[i];
	}
	
	return bag;
}


void Tester::loadData(ifstream *input){
	BackpackLoader bploader;
	string data;
	int iterator = 0;
	while(!input->eof()){
		iterator++;
		getline(*input,data);
		if(data.size()<1) continue;
		backpacks.push_back(bploader.loadData(data));
	}
}
void Tester::loadResults(ifstream *input){
	string data;
	stringstream ss;
	Result* result;
	int id, amount, i;
	while(!input->eof()){
		getline(*input,data);
		if(data.size()<1) continue;
		ss<<data;
		ss>>id>>amount;
		result = new Result(id, amount);
		// cout<<"looking for: "<<id<<" with "<<amount<<" items"<<endl;
		for (vector<Backpack*>::iterator it = backpacks.begin() ; it != backpacks.end(); ++it){
			if(id==(*it)->id){
				if(amount!= (*it)->items_amount) throw range_error("illegal backpack capacity");
				 else{
				 	// cout<<id<<" loaded"<<endl;
				 	ss>>result->price_reached;
					for (int i = 0; i < (*it)->items_amount; ++i) {
						ss>>result->items[i];
						// cout<<result->items[i];
						}
					}
					(*it)->result = result;
			}
		}
	}
}
Tester::~Tester(){
	for (vector<Backpack*>::iterator it = backpacks.begin() ; it != backpacks.end(); ++it) delete *it;
}

void Tester::runTests(int n){
	//load results
	StupidSolver stupidSolver;
	Result* stupidResult;
	chrono::high_resolution_clock::time_point t1;
	chrono::high_resolution_clock::time_point t2;
	chrono::duration<double> time_span;
	double totalTime = 0;
	float sumDiff,diff,maxDiff;
	sumDiff=0;
	maxDiff=0;
	for (vector<Backpack*>::iterator it = backpacks.begin() ; it != backpacks.end(); ++it) {
		t1 = chrono::high_resolution_clock::now();
		stupidResult = stupidSolver.solve(*it);
		t2 = chrono::high_resolution_clock::now();
		time_span = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
		totalTime += time_span.count();
		if(!(*it)->result) throw out_of_range("no solution loaded");
		delete stupidResult;
		
	}

	cout<<totalTime<<endl;
}




Result* StupidSolver::solve(Backpack* bag){
	Result* bestResult=new Result(bag->id,bag->items_amount);
	Result* actualResult=new Result(bag->id,bag->items_amount);
	solveRecursively(bag,0,bestResult,actualResult);
	delete actualResult;
	return bestResult;
}

void StupidSolver::solveRecursively(const Backpack* bag,int begin,Result* bestResult, Result* actualResult){
	if (begin==bag->items_amount)
	{
		//count
		// cout<<actualResult->id<<" ";
		int weight=0;
		int price=0;
		for (int i = 0; i < bag->items_amount; ++i){
			if (actualResult->items[i]==1)
			{
				weight += bag->backpack_items[i];
				price += bag->items_values[i]; 
			}
			// cout<<actualResult->items[i];
		}
		
		if(weight<= bag->backpack_capacity && price > bestResult->price_reached){
			bestResult->price_reached = price; 
			for (int k = 0; k < bag->items_amount; ++k){
				bestResult->items[k] = actualResult->items[k];
			}
		}
	}else{
		actualResult->items[begin]=1;
		solveRecursively(bag,begin+1,bestResult,actualResult);
		actualResult->items[begin]=0;
		solveRecursively(bag,begin+1,bestResult,actualResult);
	}
}


int main(int argc, char const *argv[])
{

	Tester* tester = new Tester;
	ifstream input;

	cout<<"-------knap_4.inst.dat--------"<<endl;
	input.open("./data/knap_4.inst.dat");
	tester->loadData(&input);
	input.close();
	input.open("./sol/knap_4.sol.dat");
	tester->loadResults(&input);
	input.close();
	tester->runTests(4);
	delete tester;

	tester = new Tester;
	cout<<"-------knap_10.inst.dat--------"<<endl;
	input.open("./data/knap_10.inst.dat");
	tester->loadData(&input);
	input.close();
	input.open("./sol/knap_10.sol.dat");
	tester->loadResults(&input);
	input.close();
	tester->runTests(10);
	delete tester;

	tester = new Tester;
	cout<<"-------knap_15.inst.dat--------"<<endl;
	input.open("./data/knap_15.inst.dat");
	tester->loadData(&input);
	input.close();
	input.open("./sol/knap_15.sol.dat");
	tester->loadResults(&input);
	input.close();
	tester->runTests(15);
	delete tester;

	tester = new Tester;
	cout<<"-------knap_20.inst.dat--------"<<endl;
	input.open("./data/knap_20.inst.dat");
	tester->loadData(&input);
	input.close();
	input.open("./sol/knap_20.sol.dat");
	tester->loadResults(&input);
	input.close();
	tester->runTests(20);
	delete tester;
	
	tester = new Tester;
	cout<<"-------knap_22.inst.dat--------"<<endl;
	input.open("./data/knap_22.inst.dat");
	tester->loadData(&input);
	input.close();
	input.open("./sol/knap_22.sol.dat");
	tester->loadResults(&input);
	input.close();
	tester->runTests(22);
	delete tester;

	
	return 0;

}



